#ifndef __US_100_H
#define __US_100_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32g0xx_hal.h"
#include "stdint.h"
typedef struct{
uint32_t distance;
GPIO_TypeDef *Trig_Port;
uint16_t Trig_Pin;
GPIO_TypeDef *Echo_Port; 
uint16_t Echo_Pin;
float Speed;//  speed of sound
}us100_t;
void delay_us(int time);
void Init_us100(us100_t *us100,GPIO_TypeDef *Trig_Port, uint16_t Trig_Pin,GPIO_TypeDef *Echo_Port, uint16_t Echo_Pin);
uint16_t get_distance(us100_t *us100);

	#ifdef __cplusplus
}
#endif

#endif /* __US100_H */
