/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include "communication.h"
#include "US100.h"
#include "stdlib.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum
{
	WRITEMODE=0,
	CHECKADDRESSMODE=1,
	CRCMODE=2
}State_t;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define NUMSENSOR 9
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

//==== CONFIG===
uint8_t address='a'+9;
/* default config*/
float config[11]={0.3355,0.3355,0.3355,0.3355,0.3355,0.3355,0.3355,0.3355,0.3355,0,10}; 
uint8_t configInd=0;
//==== RS485===
uint8_t test[13]="hello world \n";
uint8_t tranArr[100];
uint8_t receiArr[100];
uint8_t numTArr;
uint8_t numRArr;
uint8_t t_crc;
uint8_t rx_crc;
uint8_t rx_data;
State_t state=CHECKADDRESSMODE;
/*
funt=0 default 
funt=1 config
funt=2 send data
funt=3 config ERROR
funt=4 request ERROR
*/
uint8_t funt=0;
uint8_t request=0;
uint8_t stateConfig=0;
uint8_t Ok=253;
const uint8_t sep_char=';';
const uint8_t start_char='{';
const uint8_t end_char='}';
uint8_t fc_config_char='-';
uint8_t fc_request_char='=';
//=== US100===
us100_t sensor[9];
uint16_t distance[9]={0};// cm
uint16_t time_delay=10;//ms
uint8_t mode=0;
uint8_t read_permission=0;
uint8_t latch=99;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void creat_arr(uint16_t ip_num,uint8_t *op_arr, uint8_t num_sensor , uint8_t *num_char);
uint8_t Crc8(uint8_t *data, int len);
void clearArr(uint8_t *arr,uint8_t num);

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

		if(huart->Instance == USART1)
	{
	
			if(state==CRCMODE)
			{
				receiArr[numRArr]=rx_data;
				state=CHECKADDRESSMODE;
			}
			else
			{
				if(state==CHECKADDRESSMODE)
				{
					if(rx_data==address)
					{
						state=WRITEMODE;
							for(int k=0;k<99;k++)
							{
							*(receiArr+k)=0;
							}
							numRArr=0;
					}
				}
				else if(state==WRITEMODE)
				{
					if(rx_data==end_char)
					{
									latch=numRArr;
									state=CRCMODE;
					//protect crc== endchar
					}
				receiArr[numRArr]=rx_data;
				numRArr++;	
				}
			}

			HAL_UART_Receive_IT(&huart1, (uint8_t*)&rx_data, 1);
	}

}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
 {
  /* USER CODE BEGIN 1 */
	for(int i=0;i<9;i++ )
	{
sensor[i].Speed=config[i];
	}
	mode=config[9];
	time_delay=config[10];
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
	HAL_UART_Receive_IT(&huart1, (uint8_t*)&rx_data, 1);
	HAL_TIM_Base_Start(&htim3);
Init_us100(sensor,trig1_GPIO_Port,trig1_Pin,echo1_GPIO_Port, echo1_Pin);
Init_us100(&sensor[1],trig2_GPIO_Port,trig2_Pin,echo2_GPIO_Port, echo2_Pin);
Init_us100(&sensor[2],trig3_GPIO_Port,trig3_Pin,echo3_GPIO_Port, echo3_Pin);
Init_us100(&sensor[3],trig4_GPIO_Port,trig4_Pin,echo4_GPIO_Port, echo4_Pin);
Init_us100(&sensor[4],trig5_GPIO_Port,trig5_Pin,echo5_GPIO_Port, echo5_Pin);
Init_us100(&sensor[5],trig6_GPIO_Port,trig6_Pin,echo6_GPIO_Port, echo6_Pin);
Init_us100(&sensor[6],trig7_GPIO_Port,trig7_Pin,echo7_GPIO_Port, echo7_Pin);
Init_us100(&sensor[7],trig8_GPIO_Port,trig8_Pin,echo8_GPIO_Port, echo8_Pin);
Init_us100(&sensor[8],trig9_GPIO_Port,trig9_Pin,echo9_GPIO_Port, echo9_Pin);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  uint8_t buffer[10] = {0xff};
  uint8_t numBuff = 0;
  uint8_t door = 0;
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
		if(receiArr[latch+1]!=0)// if receive crc8
	{

 			rx_crc=Crc8(receiArr,latch+1);
		
				if(receiArr[0]==fc_config_char)
				{
						if(receiArr[latch+1]==rx_crc)
						{
							funt=1;
						}
				}
				if(receiArr[0]==fc_request_char)
				{
					if(receiArr[latch+1]==0x64) // crc funt 2=0x64
					{
				funt=2;
					}
				}
		latch=99;
	}
  switch (funt)
  {
  case 1:
    for (int i = 0; i < 100; i++)
    {
      if (receiArr[i] == start_char)
      {
        door = 1;
      }
      else if (receiArr[i] > 45 && receiArr[i] < 58 && door == 1)
      {
        buffer[numBuff] = receiArr[i];
        numBuff++;
      }
      else if ((receiArr[i] == end_char || receiArr[i] == sep_char) && door == 1)
      {
        config[configInd] = atof((char *)buffer);
        configInd++;
        numBuff = 0;
        for (int j = 10; j; j--)
        {
          buffer[j] = 0xff;
        }
      }
      if (receiArr[i] == end_char)
      {
        door = 0;
        configInd = 0;
      }
    }
			HAL_Delay(110);// wait rx master is ready
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
			HAL_UART_Transmit(&huart1,&address, 1, 100);
			HAL_UART_Transmit(&huart1, &Ok, 1, 100);
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
    for (int i = 0; i < 9; i++)
    {
      sensor[i].Speed = config[i];
    }
    mode = config[9];
    time_delay = config[10];
			for(int k=0;k<99;k++)
				{
				*(receiArr+k)=0;
				}
    funt = 0;
    break;
  case 2:
    request = 1;
		HAL_Delay(10);// wait rx master is ready
									for(int k=0;k<99;k++)
				{
				*(receiArr+k)=0;
				}
    funt = 0;
    break;
  case 0:
				HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
			
    switch (mode)
    {
    case 0:
      // statements
      for (int i = 0; i < 9; i++)
      {
        distance[i] = get_distance(&sensor[i]);
				creat_arr(distance[i], tranArr, 9, &numTArr);
        HAL_Delay(time_delay);
      }
      if (request == 1)
      {

        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
        t_crc = Crc8(tranArr, (int)numTArr);
        HAL_UART_Transmit(&huart1, tranArr, numTArr, 100);
        HAL_UART_Transmit(&huart1, &t_crc, 1, 100);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
        request = 0;
      }
      break;
    case 1:
      if (request == 1)
      {

        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
        for (int i = 0; i < 9; i++)
        {
          distance[i] = get_distance(&sensor[i]);
          HAL_Delay(time_delay);
					creat_arr(distance[i], tranArr, 9, &numTArr);
        }
        t_crc = Crc8(tranArr, (int)numTArr);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_SET);
        HAL_UART_Transmit(&huart1, tranArr, numTArr, 100);
        HAL_UART_Transmit(&huart1, &t_crc, 1, 100);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, GPIO_PIN_RESET);
				request = 0;
      }
      break;

      // default statements
    }
		break;
  }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 16;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, trig11_Pin|trig6_Pin|trig9_Pin|trig10_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, trig12_Pin|trig7_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, trig4_Pin|trig1_Pin|trig3_Pin|trig5_Pin
                          |trig2_Pin|trig8_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(de1_GPIO_Port, de1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : trig11_Pin trig6_Pin trig9_Pin trig10_Pin */
  GPIO_InitStruct.Pin = trig11_Pin|trig6_Pin|trig9_Pin|trig10_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : echo12_Pin */
  GPIO_InitStruct.Pin = echo12_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(echo12_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : trig12_Pin trig7_Pin */
  GPIO_InitStruct.Pin = trig12_Pin|trig7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : echo4_Pin echo1_Pin echo3_Pin echo5_Pin
                           echo7_Pin echo8_Pin echo9_Pin */
  GPIO_InitStruct.Pin = echo4_Pin|echo1_Pin|echo3_Pin|echo5_Pin
                          |echo7_Pin|echo8_Pin|echo9_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : trig4_Pin de1_Pin */
  GPIO_InitStruct.Pin = trig4_Pin|de1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : trig1_Pin */
  GPIO_InitStruct.Pin = trig1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
  HAL_GPIO_Init(trig1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : trig3_Pin trig5_Pin trig2_Pin trig8_Pin */
  GPIO_InitStruct.Pin = trig3_Pin|trig5_Pin|trig2_Pin|trig8_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : echo6_Pin echo2_Pin echo10_Pin echo11_Pin */
  GPIO_InitStruct.Pin = echo6_Pin|echo2_Pin|echo10_Pin|echo11_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void creat_arr(uint16_t ip_num, uint8_t *op_arr, uint8_t num_sensor, uint8_t *num_char)
{
  op_arr[0] = '{';
  static uint8_t ind_op = 1;
  uint8_t numberOfDigits = 0;
  uint16_t check = ip_num;
  static uint8_t dem = 0;
  dem++;
  while (check != 0)
  {
    check = check / 10;
    numberOfDigits++;
  }
  int temp[numberOfDigits];
  for (int i = 0; i < numberOfDigits; i++)
  {
    int mu = numberOfDigits - i - 1;
    temp[i] = ip_num / (pow(10, mu));
  }

  op_arr[ind_op] = temp[0] + 48;
  ind_op++;

  for (int j = 1; j < numberOfDigits; j++, ind_op++)
  {
    op_arr[ind_op] = (uint8_t)(temp[j] - temp[j - 1] * 10) + 48;
  }

  if (dem < num_sensor)
  {
    op_arr[ind_op] = ';';
    ind_op++;
  }
  else if (dem == num_sensor)
  {
    op_arr[ind_op] = '}';
    ind_op++;
    *num_char = ind_op;
    ind_op = 1;
		dem=0;
  }
}
uint8_t Crc8(uint8_t *data, int len)
{
  unsigned crc = 0;
  int i, j;
  for (j = len; j; j--, data++)
  {
    crc ^= (*data << 8);
    for (i = 8; i; i--)
    {
      if (crc & 0x8000)
        crc ^= (0x1070 << 3);
      crc <<= 1;
    }
  }
  return (uint8_t)(crc >> 8);
}
void clearArr(uint8_t *arr,uint8_t num)
{
for(int i;i<num;i++)
	{
	arr[i]=0;
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
