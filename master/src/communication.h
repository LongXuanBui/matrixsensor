
#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H
#include <Arduino.h>
#include <stdlib.h>
#include <cstring>
#define DE 33
enum Cstatus
{
  LOCK = 0,
  OPEN,
  SUCCESS,
  CRC8MODE
}; // 4 status when receiving
#define NUMSLAVE 2
uint8_t address[NUMSLAVE];
void init_rs485()
{
  Serial1.begin(9600, SERIAL_8N1, 25, 32);
  pinMode(DE, OUTPUT);
  digitalWrite(DE, LOW);
  Serial.print("init RS485 success \n");
}
uint8_t Crc8(uint8_t *data, int len)
{

  unsigned crc = 0;
  int i, j;
  for (j = len; j; j--, data++)
  {
    crc ^= (*data << 8);
    for (i = 8; i; i--)
    {
      if (crc & 0x8000)
        crc ^= (0x1070 << 3);
      crc <<= 1;
    }
  }
  return (uint8_t)(crc >> 8);
}
void configStm(char *config)
{
  long times = 0;
  long offsetTimes = 8000;
  long curentTime = 0;
  char rx_data;
  bool add_check = false;
  bool config_state = false;
  char fun = '-';
  const char ok = 253;
  uint8_t crc_buff[70];
  int crc_size = 0;
  uint8_t crc;

  Serial.println("Create address");
  Serial.println("Address: ");
  for (int i = 0; i < NUMSLAVE; i++)
  {
    address[i] = 'a'+8+ i;//a=97
    Serial.write(address[i]);
    Serial.print("\t");
  }
  Serial.println("=== ADDRESS===");
  crc_buff[0] = fun;
  for (int i = 0; i < NUMSLAVE; i++)
  {
    times = millis();
    digitalWrite(DE, HIGH);
    Serial1.write(address[i]);
    Serial1.write(fun);
    for (int j = 0; j < 70; j++)
    {
      if (*(config + i * 70 + j) != NULL)
      {
        Serial1.write(*(config + i * 70 + j));
        crc_buff[j + 1] = *(config + i * 70 + j);
        crc_size++;
      }
    }
    crc = Crc8((crc_buff), crc_size + 1);
    Serial1.write(crc);
    crc_size = 0;
    delay(100);
    digitalWrite(DE, LOW);

    while (curentTime < (times + offsetTimes))
    {
      curentTime = millis();
      if (Serial1.available() > 0)
      {
        rx_data = Serial1.read();
        if (rx_data == address[i])
        {
          add_check = true;
        }
        if (rx_data == ok && add_check == true)
        {
          config_state = true;
        }
      }
      if ((curentTime == (times + offsetTimes / 4) || curentTime == (times + offsetTimes / 2) || curentTime == (times + (offsetTimes * 3) / 4)) && config_state != true)
      {
        digitalWrite(DE, HIGH);
        Serial1.write(address[i]);
        Serial1.write(fun);

        for (int j = 0; j < 70; j++)
        {
          if (*(config + i * 70 + j) != NULL)
          {
            Serial1.write(*(config + i * 70 + j));
            crc_buff[j + 1] = *(config + i * 70 + j);
            crc_size++;
          }
        }
        crc = Crc8((crc_buff), crc_size + 1);
        Serial1.write(crc);
        crc_size = 0;
        delay(100);
        digitalWrite(DE, LOW);
      }
    }
    if (config_state == true)
    {
    
      config_state = false;
    }
    else
    {
      Serial.print("Slave ");
      Serial.print(i + 1);
      Serial.println("Can't config !");
    }
      add_check = false;
      delay(100);
  }
}
void getAll(int *arr, unsigned long offsetTimes)
{

  unsigned long times = 0;
  unsigned long curentTimes = 0;
  // period to read a slave
  Cstatus status = LOCK;
  int arr_ind = 0;
  const char sep_char = ';';   // separator character
  const char start_char = '{'; // start character
  const char end_char = '}';   // end character
  char rx_data = 0xff;
  char buffer[6] = {0};
  int buffer_ind = 0;
  uint8_t crcT;
  uint8_t crcR;
  uint8_t crc_buffer[70] = {0}; // buffer stores received characters
  uint8_t numBytes = 0;         // count num of crc buffer
  const char fun = '=';
  for (int i = 0; i < NUMSLAVE; i++)
  {
    times = millis();
    digitalWrite(DE, HIGH);
    Serial1.write(address[i]);
    Serial1.write(fun);
    crcT = 0x64;
    Serial1.write(end_char);
    Serial1.write(crcT);
    delay(5);
    digitalWrite(DE, LOW);
    numBytes = 0;
    while (curentTimes < (times + offsetTimes))
    {
      curentTimes = millis();
      if (Serial1.available() > 0)
      {
        rx_data = Serial1.read();
        if (rx_data == start_char)
        {
          status = OPEN;
          crc_buffer[numBytes] = rx_data;
          numBytes++;
        }
        if (status == OPEN && rx_data != start_char)
        {
          crc_buffer[numBytes] = rx_data;
          numBytes++;

          if (rx_data == end_char)
          {
            status = CRC8MODE;
          }
        }
        if (status == CRC8MODE && rx_data != end_char)
        {
          crcR = rx_data;
          if (crcR == Crc8(crc_buffer, numBytes))
          {
            status = SUCCESS;
            break;
          }
        }
      }
      if (status != SUCCESS && (curentTimes == (times + offsetTimes / 4) || curentTimes == (times + offsetTimes / 2) || curentTimes == (times + (offsetTimes * 3) / 4)))
      {
        digitalWrite(DE, HIGH);
        Serial1.write(address[i]);
        Serial1.write(fun);
        crcT = 0x64;
        Serial1.write(end_char);
        Serial1.write(crcT);
        delay(5);
        digitalWrite(DE, LOW);
        Serial.print("Slave ");
        Serial.print(i + 1);
        Serial.println(" No connection or data frame error, try connecting again!");
      }
    }
    if (status == SUCCESS)
    {

      for (int j = 0; j < numBytes; j++)
      {
        if (crc_buffer[j] == start_char)
        {
          status = OPEN;
        }
        if (status == OPEN && crc_buffer[j] != start_char)
        {
          if (crc_buffer[j] == sep_char || crc_buffer[j] == end_char)
          {
            arr[i * 9 + arr_ind] = atoi(buffer);
            arr_ind++;
            buffer_ind = 0;
            for (int k = 0; k < 6; k++)
            {
              buffer[k] = 0;
            }
          }
          if (crc_buffer[j] != sep_char && crc_buffer[j] != end_char && crc_buffer[j] > 47 && crc_buffer[j] < 58)
          {
            buffer[buffer_ind] = crc_buffer[j];
            buffer_ind++;
          }
          if (crc_buffer[j] == end_char) // if receive '}' move to CRCmode
          {
            status = LOCK;
            arr_ind = 0;
          }
        }
      }
    }
  }
  status = LOCK;
  numBytes = 0;
  delay(100);
}
void sendMessage(char *c)
{
  digitalWrite(DE, HIGH);
  Serial1.println(c);
  delay(100);
  digitalWrite(DE, LOW);
}

 #endif