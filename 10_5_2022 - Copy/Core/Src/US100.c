#include "US100.h"
extern TIM_HandleTypeDef htim3;
int check=9;
void delay_1us()
{
 __HAL_TIM_SetCounter(&htim3, 0);
 while (__HAL_TIM_GetCounter(&htim3)< 1);
}
void delay_us(int time)
{
 int i = 0;
 for(i = 0; i < time; i++)
 {
   delay_1us();
 }
}
void Init_us100(us100_t *us100,GPIO_TypeDef *Trig_Port, uint16_t Trig_Pin,GPIO_TypeDef *Echo_Port, uint16_t Echo_Pin)
{
	us100->Echo_Pin=Echo_Pin;
	us100->Echo_Port=Echo_Port;
	us100->Trig_Port=Trig_Port;
	us100->Trig_Pin=Trig_Pin;
	
}
uint16_t get_distance(us100_t *us100)
{
HAL_GPIO_WritePin(us100->Trig_Port,us100->Trig_Pin,GPIO_PIN_RESET);
	uint16_t timeout=22000;
 uint16_t local_time;
  // pull the TRIG pin LOW

	HAL_GPIO_WritePin(us100->Trig_Port,us100->Trig_Pin,GPIO_PIN_SET);
 delay_us(5);  // wait for 2 us
 
HAL_GPIO_WritePin(us100->Trig_Port,us100->Trig_Pin,GPIO_PIN_RESET);// pull the TRIG pin HIGH
delay_us(10);  // wait for 10 us
  // pull the TRIG pin low
HAL_GPIO_WritePin(us100->Trig_Port,us100->Trig_Pin,GPIO_PIN_SET);

 // read the time for which the pin is high

 while (HAL_GPIO_ReadPin(us100->Echo_Port,us100->Echo_Pin))
 {
 local_time++;   // measure time for which the pin is high
  delay_us(1);
		if(local_time>timeout)
		return 0;
		}
	 // wait for the ECHO pin to go high
  local_time=0;
 while (!(HAL_GPIO_ReadPin(us100->Echo_Port,us100-> Echo_Pin)))    // while the pin is high
  {
  local_time++;   // measure time for which the pin is high
  delay_us(1);
		if(local_time>timeout)
		{
		return 0;
		}
  }
	
 return local_time*us100->Speed;
}
