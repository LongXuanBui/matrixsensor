/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define trig11_Pin GPIO_PIN_9
#define trig11_GPIO_Port GPIOB
#define echo12_Pin GPIO_PIN_14
#define echo12_GPIO_Port GPIOC
#define trig12_Pin GPIO_PIN_15
#define trig12_GPIO_Port GPIOC
#define echo4_Pin GPIO_PIN_0
#define echo4_GPIO_Port GPIOA
#define trig4_Pin GPIO_PIN_1
#define trig4_GPIO_Port GPIOA
#define echo1_Pin GPIO_PIN_2
#define echo1_GPIO_Port GPIOA
#define trig1_Pin GPIO_PIN_3
#define trig1_GPIO_Port GPIOA
#define echo3_Pin GPIO_PIN_4
#define echo3_GPIO_Port GPIOA
#define trig3_Pin GPIO_PIN_5
#define trig3_GPIO_Port GPIOA
#define echo5_Pin GPIO_PIN_6
#define echo5_GPIO_Port GPIOA
#define trig5_Pin GPIO_PIN_7
#define trig5_GPIO_Port GPIOA
#define echo6_Pin GPIO_PIN_0
#define echo6_GPIO_Port GPIOB
#define trig6_Pin GPIO_PIN_1
#define trig6_GPIO_Port GPIOB
#define echo2_Pin GPIO_PIN_2
#define echo2_GPIO_Port GPIOB
#define trig2_Pin GPIO_PIN_8
#define trig2_GPIO_Port GPIOA
#define echo7_Pin GPIO_PIN_9
#define echo7_GPIO_Port GPIOA
#define trig7_Pin GPIO_PIN_6
#define trig7_GPIO_Port GPIOC
#define echo8_Pin GPIO_PIN_10
#define echo8_GPIO_Port GPIOA
#define trig8_Pin GPIO_PIN_11
#define trig8_GPIO_Port GPIOA
#define de1_Pin GPIO_PIN_12
#define de1_GPIO_Port GPIOA
#define echo9_Pin GPIO_PIN_15
#define echo9_GPIO_Port GPIOA
#define trig9_Pin GPIO_PIN_3
#define trig9_GPIO_Port GPIOB
#define echo10_Pin GPIO_PIN_4
#define echo10_GPIO_Port GPIOB
#define trig10_Pin GPIO_PIN_5
#define trig10_GPIO_Port GPIOB
#define echo11_Pin GPIO_PIN_8
#define echo11_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
