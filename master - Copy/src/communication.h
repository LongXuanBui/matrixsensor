
#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H
#include <Arduino.h>
void init_rs485()
{
  Serial1.begin(9600, SERIAL_8N1, 25, 32);
  pinMode(33, OUTPUT);
  digitalWrite(33, LOW);
  Serial.print("init RS485 success \n");
}
void sendMessage(char *c)
{
  digitalWrite(33, HIGH);
  Serial1.println(c);
  delay(100);
  digitalWrite(33, LOW);
}
void sendAChar(char c)
{
  digitalWrite(33, HIGH);
  Serial1.write(c);
  delay(100);
  digitalWrite(33, LOW);
}
 #endif