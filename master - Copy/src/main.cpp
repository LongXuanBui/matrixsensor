#include <Arduino.h>
#include "communication.h"
#include <string.h>
#define NUM 8
char message[13] = "hello world:";
char add[NUM] = {0};
int setTime = 3000; // ms
int currentTime = 0;
int preTime = 0;
void setup()
{

  Serial.begin(115200);
  init_rs485();
  pinMode(18, OUTPUT);
  digitalWrite(18, LOW); // led status
  // sendMessage(message);
  for (int i = 0; i < NUM; i++)
  {
    add[i] = 'a' + i;
  }

  Serial.println("End config waiting...");
  Serial.println();
}
void loop()
{
  for (int i = 0; i < NUM; i++)
  {
    sendAChar(add[i]);
    preTime = millis();
    while (currentTime < (preTime + setTime))
    {
      currentTime = millis();
      if (Serial1.available())
      {
        char rx_data = Serial1.read();
        Serial.write(rx_data);
      }
    }
    Serial.println();
    delay(1000);
  }
  delay(5000);
}
